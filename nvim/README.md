# Install

1. Run this script (Press enter if error):

```sh
rm -rf ~/.config/nvim ~/.local/share/nvim ~/.cache/nvim && git clone https://github.com/kidp2h/lazynvim.git ~/.config/nvim --depth 1 && nvim

```

2. Run this command in nvim (Press enter if error):

```sh
:MasonInstallAll
```

3. Reboot nvim and run:

```
:PackerSync
```
